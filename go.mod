module github.com/mcrute/alpine-images-service

go 1.15

require (
	github.com/aws/aws-sdk-go v1.36.26
	github.com/hashicorp/vault/api v1.0.4 // indirect
	gopkg.in/square/go-jose.v2 v2.5.1
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
)
