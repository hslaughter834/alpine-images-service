package broker_api

import (
	"crypto/ecdsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"
)

type Account struct {
	ShortName           string `json:"short_name"`
	Name                string `json:"name"`
	AccountNumber       int    `json:"account_number"`
	ConsoleRedirectURL  string `json:"console_redirect_url"`
	CredentialsURL      string `json:"credentials_url"`
	GetConsoleURL       string `json:"get_console_url"`
	GlobalCredentialURL string `json:"global_credential_url"`
	client              *BrokerAPIClient
}

func (a *Account) GetRegions() ([]*Region, error) {
	out := []*Region{}

	if err := a.client.apiGet(a.CredentialsURL, &out); err != nil {
		return nil, err
	}

	for _, r := range out {
		r.client = a.client
	}

	return out, nil
}

type Region struct {
	Name           string `json:"name"`
	Enabled        bool   `json:"enabled"`
	CredentialsURL string `json:"credentials_url"`
	client         *BrokerAPIClient
}

func (r *Region) GetAWSSession() (*session.Session, error) {
	k, err := r.GetAccessKey()
	if err != nil {
		return nil, err
	}

	s := session.New(&aws.Config{
		Region: aws.String(r.Name),
		Credentials: credentials.NewStaticCredentials(
			k.AccessKey,
			k.SecretKey,
			k.SessionToken,
		),
	})

	return s, nil
}

func (r *Region) GetAccessKey() (*AccessKey, error) {
	out := &AccessKey{}

	if err := r.client.apiGet(r.CredentialsURL, &out); err != nil {
		return nil, err
	}

	return out, nil
}

type AccessKey struct {
	AccessKey    string `json:"access_key"`
	SecretKey    string `json:"secret_key"`
	SessionToken string `json:"session_token"`
	Expiration   string `json:"expiration"`
}

type BrokerAPIClient struct {
	endpoint *url.URL
	subject  string
	client   *http.Client
	signer   jose.Signer
}

func NewBrokerAPIClient(endpoint, privateKey, subject string) (*BrokerAPIClient, error) {
	private, err := decodePrivateKey(privateKey)
	if err != nil {
		return nil, err
	}

	pu, err := url.Parse(endpoint)
	if err != nil {
		return nil, err
	}

	signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.ES256, Key: private}, nil)
	if err != nil {
		return nil, err
	}

	return &BrokerAPIClient{pu, subject, &http.Client{}, signer}, nil
}

func decodePrivateKey(k string) (*ecdsa.PrivateKey, error) {
	d, err := base64.StdEncoding.DecodeString(k)
	if err != nil {
		return nil, err
	}

	pk, err := x509.ParseECPrivateKey(d)
	if err != nil {
		return nil, err
	}

	return pk, nil
}

func (c *BrokerAPIClient) GetAccounts() (map[string]*Account, error) {
	url := *c.endpoint
	url.Path = "/api/account"

	accounts := []*Account{}
	err := c.apiGet(url.String(), &accounts)
	if err != nil {
		return nil, err
	}

	out := map[string]*Account{}
	for _, a := range accounts {
		a.client = c
		out[a.ShortName] = a
	}

	return out, nil
}

func (c *BrokerAPIClient) apiGet(url string, model interface{}) error {
	// TODO: Remove this!
	url = strings.Replace(url, "https://", "http://", 1)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return err
	}

	now := time.Now()
	token, err := jwt.Signed(c.signer).Claims(jwt.Claims{
		Issuer:    c.subject,
		Subject:   c.subject,
		Audience:  jwt.Audience{"aws-access"},
		Expiry:    jwt.NewNumericDate(now.Add(5 * time.Minute)),
		NotBefore: jwt.NewNumericDate(now),
		IssuedAt:  jwt.NewNumericDate(now),
	}).CompactSerialize()
	if err != nil {
		return err
	}

	req.Header.Add("X-Service-Key", token)

	res, err := c.client.Do(req)
	if err != nil {
		return err
	}

	// TODO: HTTP 429 is a rate limit, handle this

	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("Error status: %d", res.StatusCode)
	}

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return err
	}

	if err = json.Unmarshal(body, model); err != nil {
		return err
	}

	return nil
}
