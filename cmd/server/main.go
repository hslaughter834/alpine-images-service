package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/mcrute/alpine-images-service/broker_api"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	vault "github.com/hashicorp/vault/api"
	"gopkg.in/yaml.v3"
)

type ImageError struct {
	Region  string
	ImageId string
	Err     error
}

func NewImageError(region, id string, msg string, fields ...interface{}) error {
	return &ImageError{
		Region:  region,
		ImageId: id,
		Err:     fmt.Errorf(msg, fields...),
	}
}

func (e *ImageError) Error() string {
	return fmt.Sprintf("%s (region: %s) (image: %s)", e.Err.Error(), e.Region, e.ImageId)
}

func (e *ImageError) Unrwap() error {
	return e.Err
}

type ImageIndex struct {
	m       sync.RWMutex
	idx     map[string]map[string]map[string]*Image
	nameIdx map[string]*Image
}

func NewImageIndex() *ImageIndex {
	return &ImageIndex{
		idx:     map[string]map[string]map[string]*Image{},
		nameIdx: map[string]*Image{},
	}
}

func (i *ImageIndex) MarshalJSON() ([]byte, error) {
	return json.Marshal(i.idx)
}

func (i *ImageIndex) MarshalYAML() (interface{}, error) {
	return i.idx, nil
}

func (i *ImageIndex) ensurePath(image *Image) {
	if _, ok := i.idx[image.Release]; !ok {
		i.idx[image.Release] = map[string]map[string]*Image{}
	}

	if _, ok := i.idx[image.Release][image.ProfileBuild]; !ok {
		i.idx[image.Release][image.ProfileBuild] = map[string]*Image{}
	}
}

func (i *ImageIndex) Get(name string) *Image {
	i.m.RLock()
	defer i.m.RUnlock()

	if img, ok := i.nameIdx[name]; !ok {
		return nil
	} else {
		return img
	}
}

func (i *ImageIndex) Put(image *Image) {
	i.m.Lock()
	defer i.m.Unlock()

	i.ensurePath(image)

	i.idx[image.Release][image.ProfileBuild][image.Name] = image
	i.nameIdx[image.Name] = image
}

func (i *ImageIndex) DeleteImage(image *Image) {
	i.m.Lock()
	defer i.m.Unlock()

	if _, ok := i.nameIdx[image.Name]; ok {
		delete(i.nameIdx, image.Name)
	}

	delete(i.idx[image.Release][image.ProfileBuild], image.Name)
}

func (i *ImageIndex) DeleteName(n string) {
	i.m.RLock()
	ni, ok := i.nameIdx[n]
	i.m.RUnlock()

	if ok {
		i.DeleteImage(ni)
	}
}

// Remove build_time (seems like it was just the unix version of creation_date)
// Normalize creation_date and end_of_life string formats
//
// BuildDate is the earliest build date of any image in any region. This is
// assumed to be the actual build date of the image in the source region
// independent of any replication delay.
type Image struct {
	ImageId            string            `json:"-" yaml:"-"`
	Region             string            `json:"-" yaml:"-"`
	Name               string            `json:"name" yaml:"name"`
	Description        string            `json:"description" yaml:"description"`
	Profile            string            `json:"profile" yaml:"profile"`
	ProfileBuild       string            `json:"profile_build" yaml:"profile_build"`
	Version            string            `json:"version" yaml:"version"`
	Release            string            `json:"release" yaml:"release"`
	Architecture       string            `json:"arch" yaml:"arch"`
	Revision           string            `json:"revision" yaml:"revision"`
	VirtualizationType string            `json:"virt_type" yaml:"virt_type"`
	BuildDate          *time.Time        `json:"build_date" yaml:"build_date"`
	CreationDate       *time.Time        `json:"creation_date" yaml:"creation_date"`
	EndOfLife          *time.Time        `json:"end_of_life" yaml:"end_of_life"`
	Artifacts          map[string]string `json:"artifacts" yaml:"artifacts"` // Region, ImageId
}

func (i *Image) IsExpired() bool {
	return i.EndOfLife.Before(time.Now())
}

// UpdateBuildDate updates the build date to be the earlier of the current
// build date or the passed date. Correctly handles null.
func (i *Image) UpdateBuildDate(t *time.Time) {
	if t == nil {
		return
	}

	if i.BuildDate == nil && t != nil {
		i.BuildDate = t
		return
	}

	if t.Before(*i.BuildDate) {
		i.BuildDate = t
	}
}

func ImageFromAWSImage(region string, img *ec2.Image) (*Image, error) {
	if *img.State != "available" {
		return nil, NewImageError(region, *img.ImageId, "Skipping image state '%s'", *img.State)
	}

	if !*img.Public {
		return nil, NewImageError(region, *img.ImageId, "Skipping non-public image")
	}

	// We require this but the API doesn't mandate that it exists since
	// it's a tag. The only reason we should hit this is if our upstream
	// automation screwed up something.
	if img.Name == nil {
		return nil, NewImageError(region, *img.ImageId, "Skipping image without name")
	}

	tags := http.Header{}
	for _, t := range img.Tags {
		tags.Add(*t.Key, *t.Value)
	}

	cdt := pointerToString(img.CreationDate)
	ct, err := time.Parse(time.RFC3339, cdt)
	if err != nil {
		return nil, NewImageError(region, *img.ImageId, "Error parsing creation date: '%s'", cdt)
	}

	eold := tags.Get("end_of_life")
	ed, err := time.Parse("2006-01-02T15:04:05", eold)
	if err != nil {
		return nil, NewImageError(region, *img.ImageId, "Error parsing EOL date: '%s'", eold)
	}

	// Map AWS architectures to Alpine architecture. Not sure if this is really
	// needed but this maps to the original yaml file format.
	arch := *img.Architecture
	if arch == "arm64" {
		arch = "aarch64"
	}

	return &Image{
		Name:               pointerToString(img.Name),
		Architecture:       *img.Architecture,
		VirtualizationType: *img.VirtualizationType,
		Description:        pointerToString(img.Description),
		Profile:            tags.Get("profile"),
		ProfileBuild:       tags.Get("profile_build"),
		Version:            tags.Get("version"),
		Release:            tags.Get("release"),
		Revision:           tags.Get("revision"),
		CreationDate:       &ct,
		EndOfLife:          &ed,
		Artifacts:          map[string]string{region: *img.ImageId},
	}, nil
}

func pointerToString(i *string) string {
	if i == nil {
		return ""
	}
	return *i
}

func IndexImagesInRegion(e *ec2.EC2, idx *ImageIndex, region string, skipExpired bool) (mainErr error, errs []error) {
	errs = []error{}

	il, err := e.DescribeImages(&ec2.DescribeImagesInput{
		Owners: aws.StringSlice([]string{"self"}),
	})
	if err != nil {
		mainErr = err
		return
	}

	for _, img := range il.Images {
		mi, err := ImageFromAWSImage(region, img)
		if err != nil {
			errs = append(errs, err)
			continue
		}

		if mi.IsExpired() && skipExpired {
			continue
		}

		if ci := idx.Get(mi.Name); ci != nil {
			ci.Artifacts[region] = mi.Artifacts[region]
			ci.UpdateBuildDate(mi.CreationDate)
		} else {
			idx.Put(mi)
		}
	}

	return
}

type VaultClient struct {
	client *vault.Client
}

// TODO: Our internal certs make go sad. Should fix them and remove this
func NewInsecureVaultClient(url string) (*VaultClient, error) {
	cfg := &vault.Config{Address: url}
	if err := cfg.ConfigureTLS(&vault.TLSConfig{
		Insecure: true,
	}); err != nil {
		return nil, err
	}

	client, err := vault.NewClient(cfg)
	if err != nil {
		return nil, err
	}

	vaultToken := os.Getenv("VAULT_TOKEN")
	if vaultToken == "" {
		return nil, fmt.Errorf("Vault token env variable is required")
	}
	client.SetToken(vaultToken)

	return &VaultClient{client}, nil
}

func (c *VaultClient) GetStringSecretKey(path string) (string, error) {
	secret, err := c.client.Logical().Read(path)
	if err != nil {
		return "", err
	}

	d, ok := secret.Data["data"]
	if !ok {
		return "", fmt.Errorf("No 'data' key in secret response")
	}

	dd, ok := d.(map[string]interface{})
	if !ok {
		return "", fmt.Errorf("Invalid type for secret data")
	}

	ds, ok := dd["key"]
	if !ok {
		return "", fmt.Errorf("No 'key' key in secret response")
	}

	return ds.(string), nil
}

func main() {
	brokerUrl := flag.String("broker-url", "https://aws-access.crute.us/", "Full URL for cloud identity broker")
	brokerServiceId := flag.String("broker-service-id", "service-alpine-amis", "Service ID for identity broker")
	vaultUrl := flag.String("vault-url", "https://vault.sea4.crute.me:8200/", "Full URL for Vault server")
	brokerKeyPath := flag.String("broker-key-path", "kv/data/alpine/image-service/broker-key", "Vault path for broker key")
	flag.Parse()

	vaultC, err := NewInsecureVaultClient(*vaultUrl)
	if err != nil {
		panic(err)
	}

	brokerKey, err := vaultC.GetStringSecretKey(*brokerKeyPath)
	if err != nil {
		panic(err)
	}

	c, err := broker_api.NewBrokerAPIClient(*brokerUrl, brokerKey, *brokerServiceId)
	if err != nil {
		panic(err)
	}

	a, err := c.GetAccounts()
	if err != nil {
		panic(err)
	}

	regions, err := a["alpine-amis-user"].GetRegions()
	if err != nil {
		panic(err)
	}

	idx := NewImageIndex()

	for _, region := range regions {
		s, err := region.GetAWSSession()
		if err != nil {
			panic(err)
		}

		e := ec2.New(s)

		merr, errs := IndexImagesInRegion(e, idx, region.Name, false)
		if merr != nil {
			panic(merr)
		}

		if errs != nil {
			for _, err := range errs {
				log.Println(err)
			}
		}
	}

	d, err := yaml.Marshal(idx)
	//_ = y
	//d, err := json.MarshalIndent(idx, "", "    ")
	if err != nil {
		panic(err)
	}

	fmt.Printf("%s\n", d)
}
